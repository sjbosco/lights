#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif
 
#define PIN 13
#define LED_COUNT 51
 
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_COUNT, PIN, NEO_GRB + NEO_KHZ800);

/******************************************************/
void setup() {
  Serial.begin(9600);
  strip.begin();
  strip.clear();
  strip.show(); // Initialize all pixels to 'off'
}

void loop() 
{
  for(uint16_t i=0; i<strip.numPixels(); i++)
  { strip.setPixelColor(i,strip.Color(0,0,0)); }     
  strip.show();     
}
